using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class DialogueManager : MonoBehaviour {

	//public TextMeshProUGUI nameText;
	public TextMeshProUGUI dialogueText;
	public RectTransform dailougebox;
	public RectTransform dailougePos;
	public GameObject bttnyes, bttnno;
	public Sprite popupchange1,popupchange2;
	public Button _continue;
	public static DialogueManager instance;
	[HideInInspector]
	public Vector2 orignalPopUpPos;

	//`public Animator animator;

	private Queue<SentenceStructure> sentences;
    private void Awake()
    {
		instance = this;
    }
    // Use this for initialization
    void Start () {
		orignalPopUpPos = dailougebox.anchoredPosition;
		sentences = new Queue<SentenceStructure>();
	}

	public void StartDialogue (Dialogue dialogue)
	{
		
		//animator.SetBool("IsOpen", true);
		bttnyes.SetActive(false);
		bttnno.SetActive(false);
		_continue.interactable = true;
		//nameText.text = dialogue.name;

		sentences.Clear();

        foreach (SentenceStructure text in dialogue.sentences)
        {
			

            sentences.Enqueue(text);
        }

        DisplayNextSentence();
	}

	public void DisplayNextSentence ()
	{
		if (sentences.Count == 0)
		{
			EndDialogue();
			return;
		}

		SentenceStructure Dailougesentence = sentences.Dequeue();
		if (Dailougesentence.role == "Player")
		{

			dailougebox.DOAnchorPos(dailougePos.anchoredPosition, 0f);
			//dailougebox.DOScaleX(-dailougebox.localScale.x, 0f);
			dailougebox.gameObject.GetComponent<Image>().sprite = popupchange1;
		}
		if (Dailougesentence.role == "NPC")
		{
			dailougebox.DOAnchorPos(orignalPopUpPos, 0f);
			dailougebox.gameObject.GetComponent<Image>().sprite = popupchange2;
		}
		StopAllCoroutines();
		StartCoroutine(TypeSentence(Dailougesentence.sentence));
	}

	IEnumerator TypeSentence (string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	public void EndDialogue()
	{
		dailougebox.DOScaleX(0f, 0.3f).OnComplete(onEnd);
		
		sentences.Clear();
		//sentences = new Queue<string>();
		//animator.SetBool("IsOpen", false);
	}
	void onEnd()
    {

		Shop.instance.showshop();
		//CharacterController.instance.canControl = false;
		dailougebox.DOAnchorPos(orignalPopUpPos, 0f);
		dailougebox.gameObject.GetComponent<Image>().sprite = popupchange2;
		dialogueText.text = "Would you like to purchase?";
		bttnyes.SetActive(true);
		bttnno.SetActive(true);
		_continue.interactable = false;
	}

}
