﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public Sprite[] icons;
    public Clothes[] clothes;
    public RectTransform shopPopup;
    public static Shop instance;
    public GameObject shopPlayer,originalPlayer;
    [HideInInspector]
    public bool canshowshop;
    [HideInInspector]
    public Player playerobject,orgplayerobject;
    [HideInInspector]
    public int currOutfitIndex;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        playerobject = shopPlayer.GetComponent<Player>();
        orgplayerobject = originalPlayer.GetComponent<Player>();
        if (PlayerPrefs.HasKey("OutFitIndex"))
        {
            int index = PlayerPrefs.GetInt("OutFitIndex");
           currOutfitIndex = index;
           changeoutfit(index);
           ApplyOutfit();
        }
    }
    public void shopstatus(bool can)
    {
       
            canshowshop = can;
      
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void showshop()
    {
        if (canshowshop)
        {
            shopPopup.DOScaleX(0.5838484f, 0.3f);
            CharacterController.instance.canControl = false;
        }
        else
        {
            shopPopup.DOScaleX(0f, 0.3f);
            CharacterController.instance.canControl = true;
        }
       
    }
    public void changeoutfit(int index)
    {
        Debug.Log(playerobject);
        playerobject.front.armL.GetComponent<SpriteRenderer>().sprite=clothes[index].front.armL;
        playerobject.front.armR.GetComponent<SpriteRenderer>().sprite = clothes[index].front.armR;
        playerobject.front.body.GetComponent<SpriteRenderer>().sprite = clothes[index].front.body;
        currOutfitIndex = index;
    }
    public void ApplyOutfit()
    {
        orgplayerobject.front.armL.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].front.armL;
        orgplayerobject.front.armR.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].front.armR;
        orgplayerobject.front.body.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].front.body;

        orgplayerobject.left.armL.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].left.armL;
        orgplayerobject.left.armR.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].left.armR;
        orgplayerobject.left.body.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].left.body;

        orgplayerobject.right.armL.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].left.armL;
        orgplayerobject.right.armR.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].left.armR;
        orgplayerobject.right.body.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].left.body;

        orgplayerobject.back.armL.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].back.armL;
        orgplayerobject.back.armR.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].back.armR;
        orgplayerobject.back.body.GetComponent<SpriteRenderer>().sprite = clothes[currOutfitIndex].back.body;
        PlayerPrefs.SetInt("OutFitIndex", currOutfitIndex);

    }
}
