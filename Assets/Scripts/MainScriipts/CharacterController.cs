﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public static CharacterController instance;
    #region
    [HideInInspector]
    public GameObject Player;
    public GameObject[] playerbodies;
    public GameObject[] movementsprites;
    #endregion
    [Space]
    [HideInInspector]
    public bool canControl;
    public float moveForce = 0f;
    float moveHor=0f, moveVer=0f;
    [HideInInspector]
    public int bodyIndex = 3;
    Rigidbody2D rb2d;
    float minX=-10.2f, maxX=10.2f, minY=-4.7f, maxY=2.7f;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        canControl = false;
        Player = this.gameObject;
        rb2d = this.GetComponent<Rigidbody2D>();
        bodyIndex = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (canControl)
        {
            moveHor = Input.GetAxisRaw("Horizontal");
            moveVer = Input.GetAxisRaw("Vertical");
            if (moveHor > 0)
            {
                bodyIndex = 2;
            }
            if (moveHor < 0)
            {
                bodyIndex = 1;
            }
            if (moveVer > 0)
            {
                bodyIndex = 3;
            }
            if (moveVer < 0)
            {
                bodyIndex = 0;
            }
            movementsprites[bodyIndex].SetActive(true);
        }
    }
    //10.2 -4.7, 2.7
    private void FixedUpdate()
    {
      //  Debug.Log(canControl);

        foreach (GameObject body in movementsprites)
        {
            if (body.transform.GetSiblingIndex() != bodyIndex)
            {
                body.SetActive(false);
            }
        }
       // Debug.Log(moveHor + " - " + moveVer);
      //  float x = transform.right.x * moveHor;
      //  float y=transform.up.y * moveVer;
      // float x = Mathf.Clamp(x, -10.2f, 10.2f);
     //  float y = Mathf.Clamp(x, -4.7f, 2.7f);
       // Vector3 newValues = new Vector2(x, y);
        rb2d.MovePosition(transform.position + (transform.right * moveHor+ transform.up * moveVer) * moveForce*Time.deltaTime);

    }
    private void LateUpdate()
    {
        float x = transform.position.x;
        float y = transform.position.y;
        x = Mathf.Clamp(x, minX, maxX);
        y = Mathf.Clamp(y, minY, maxY);
        transform.position = new Vector2(checkX(x), checkY(y));
    }
    float checkX(float xValue)
    {
        if (xValue < minX)
        {
            xValue = minX;
        }
        if (xValue > maxX)
        {
            xValue = maxX;
        }
        return xValue;
    }
    float checkY(float yValue)
    {
        if (yValue < minY)
        {
            yValue = minY;
        }
        if (yValue > maxY)
        {
            yValue = maxY;
        }
        return yValue;
    }
    public void Controlller(bool check)
    {
        canControl = check;
    }
}
