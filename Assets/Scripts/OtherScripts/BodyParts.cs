﻿using UnityEngine;
using UnityEditor;
[System.Serializable]
public class BodyParts
{
    public Sprite armL;
    public Sprite armR;
    public Sprite body;
}