﻿using UnityEngine;
using UnityEditor;
[System.Serializable]
public class BodyReferences 
{
    public GameObject armL, armR, body;
}