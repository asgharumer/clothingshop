﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class SentenceStructure 
{
    public string role;
    [TextArea(3, 10)]
    public string sentence;
}