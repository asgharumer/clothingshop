﻿using UnityEngine;
using UnityEditor;
[System.Serializable]
public class Clothes
{
    public BodyParts back;
    public BodyParts left;
    public BodyParts front;
}