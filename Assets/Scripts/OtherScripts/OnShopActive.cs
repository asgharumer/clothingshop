﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OnShopActive : MonoBehaviour
{
      
    // Start is called before the first frame update
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            DialogueManager.instance.dailougebox.DOScaleX(2f, 0.3f);

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        DialogueManager.instance.dailougebox.DOScaleX(0f, 0.3f);
    }
}
