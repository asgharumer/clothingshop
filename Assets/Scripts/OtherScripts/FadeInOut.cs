﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeInOut : MonoBehaviour
{
   public Image image;

    // Start is called before the first frame update
    void Start()
    {
        image.canvasRenderer.SetAlpha(1f);
        image.DOFade(0f, 2f).OnComplete(setValues);
  
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void setValues()
    {
        int index = CharacterController.instance.bodyIndex;
        CharacterController.instance.Player.transform.GetChild(index).gameObject.SetActive(true);
        CharacterController.instance.canControl = true;
    }

}
