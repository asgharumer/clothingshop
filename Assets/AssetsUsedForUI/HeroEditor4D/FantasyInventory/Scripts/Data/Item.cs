﻿using System;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Enums;
using Newtonsoft.Json;

namespace Assets.HeroEditor4D.FantasyInventory.Scripts.Data
{
    /// <summary>
    /// Represents item object for storing with game profile (please note, that item params are stored separately in params database).
    /// </summary>
    [Serializable]
    public class Item
    {
        public string Id;
        public int Count;
        public ModifierType Mod;
        public int ModLevel;

        [JsonIgnore] public ItemParams Params => GetParams(this);

        [JsonIgnore] public int Hash => $"{Id}.{Mod}.{ModLevel}".GetHashCode();

        /// <summary>
        /// This function may be overridden by the game. For example, the game may vary item params depending on Modificator.
        /// </summary>
        public static Func<Item, ItemParams> GetParams = item =>
        {
            if (ItemCollection.Instance.Dict.ContainsKey(item.Id))
            {
                return ItemCollection.Instance.Dict[item.Id];
            }

            throw new Exception($"Item params missed: {item.Id}");
        };

        public Item()
        {
        }

        public Item(string id, int count = 1)
        {
            Id = id;
            Count = count;
        }

        public Item(string id, ModifierType mod, int modLevel, int count = 1)
        {
            Id = id;
            Mod = mod;
            ModLevel = modLevel;
            Count = count;
        }

        public Item Clone()
        {
            return (Item) MemberwiseClone();
        }

        public bool IsEquipment => Params.Type == ItemType.Armor || Params.Type == ItemType.Helmet || Params.Type == ItemType.Weapon || Params.Type == ItemType.Shield;
        public bool IsShield => Params.Type == ItemType.Shield;
        public bool IsDagger => Params.Class == ItemClass.Dagger;
        public bool IsSword => Params.Class == ItemClass.Sword;
        public bool IsAxe => Params.Class == ItemClass.Axe;
        public bool IsWand => Params.Class == ItemClass.Wand;
        public bool IsBlunt => Params.Class == ItemClass.Blunt;
        public bool IsMelee => Params.Class != ItemClass.Bow;
        public bool IsBow => Params.Class == ItemClass.Bow;
        public bool IsTwoHanded => Params.Tags.Contains(ItemTag.TwoHanded);
    }
}