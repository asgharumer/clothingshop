﻿using System;
using System.Collections;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Data;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.HeroEditor4D.FantasyInventory.Scripts.Interface.Elements
{
    /// <summary>
    /// Represents inventory item and handles drag & drop operations.
    /// </summary>
    public class InventoryItem : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Image Icon;
        public Text Count;
        public Item Item;
        public Toggle Toggle;

        private float _clickTime;

        /// <summary>
        /// These actions should be set when inventory UI is opened.
        /// </summary>
        public static Action<Item> OnLeftClick;
		public static Action<Item> OnRightClick;
	    public static Action<Item> OnDoubleClick;
        public static Action<Item> OnMouseEnter;
        public static Action<Item> OnMouseExit;

        public void Start()
        {
            if (Icon != null)
            {
                Icon.sprite = IconCollection.Instance.GetIcon(Item.Params.Path);
            }

            if (Toggle)
            {
                Toggle.group = GetComponentInParent<ToggleGroup>();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            StartCoroutine(OnPointerClickDelayed(eventData));
        }

        private IEnumerator OnPointerClickDelayed(PointerEventData eventData) // TODO: A workaround. We should wait for initializing other components.
        {
            yield return null;

            OnPointerClick(eventData.button);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnMouseEnter?.Invoke(Item);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnMouseExit?.Invoke(Item);
        }

        public void OnPointerClick(PointerEventData.InputButton button)
        {
            if (button == PointerEventData.InputButton.Left)
            {
                OnLeftClick?.Invoke(Item);

                var delta = Mathf.Abs(Time.time - _clickTime);

                if (delta < 0.5f) // If double click.
                {
                    _clickTime = 0;
                    OnDoubleClick?.Invoke(Item);
                }
                else
                {
                    _clickTime = Time.time;
                }
            }
            else if (button == PointerEventData.InputButton.Right)
            {
                OnRightClick?.Invoke(Item);
            }
        }

        public void Select(bool selected)
        {
            if (Toggle != null)
            {
                Toggle.isOn = selected;

                if (selected)
                {
                    OnLeftClick?.Invoke(Item);
                }
            }
        }
    }
}