using System;
using System.Collections.Generic;
using System.Linq;
using Assets.HeroEditor4D.Common.CommonScripts;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Data;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Enums;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.HeroEditor4D.FantasyInventory.Scripts.Interface.Elements
{
    /// <summary>
    /// Scrollable item container that can display item list. Automatic vertical scrolling.
    /// </summary>
    public class ScrollInventory : ItemContainer
    {
        public ScrollRect ScrollRect;
        public GridLayoutGroup Grid;
        public GameObject ItemPrefab;
	    public GameObject ItemNoSpritesPrefab;
		public GameObject CellPrefab;
		public bool AddEmptyCells = true;
        public bool Extend;
        public Func<Item, bool> GenericFilter;

		private readonly List<ItemType> _sortByItemType = new List<ItemType>
        {
            ItemType.Currency,
            ItemType.Loot,
            ItemType.Supply,
            ItemType.Weapon,
            ItemType.Armor,
            ItemType.Helmet,
            ItemType.Shield,
            ItemType.Jewelry
        };
        private Dictionary<Item, InventoryItem> _inventoryItems = new Dictionary<Item, InventoryItem>(); // Reusing instances to reduce Instantiate() calls.
	    private List<GameObject> _emptyCells = new List<GameObject>();
	    private bool _initialized;
        private int _hash;

        public Item FirstItem => _inventoryItems.Any() ? _inventoryItems.First().Key : null;

		public void Initialize(ref List<Item> items, Item selected, bool reset = false)
        {
            base.Initialize(ref items, selected);

			if (reset) _hash = 0;
		}

        public void Initialize(ref List<Item> items, bool reset = false)
        {
            base.Initialize(ref items, items.FirstOrDefault());

            if (reset) _hash = 0;
        }

		public void SetTypeFilter(List<ItemType> types)
        {
            GenericFilter = item => types.Contains(item.Params.Type);
			Refresh(null, force: true);
        }

        public override void Refresh(Item selected)
        {
            Refresh(selected, force: false);
        }

        public void Refresh(Item selected, bool force)
		{
		    if (Items == null) return;

            var inventoryItems = new Dictionary<Item, InventoryItem>();
	        var emptyCells = new List<GameObject>();
			var items = Items.OrderBy(i => _sortByItemType.Contains(i.Params.Type) ? _sortByItemType.IndexOf(i.Params.Type) : 0).ToList();
            var groups = items.GroupBy(i => i.Params.Type);

            items = new List<Item>();

            foreach (var group in groups)
            {
                items.AddRange(group.OrderBy(i => i.Params.Price));
            }

            if (GenericFilter != null)
            {
                items.RemoveAll(i => !GenericFilter(i));
			}

            if (!force && _initialized && _hash == JsonConvert.SerializeObject(items).GetHashCode()) return;

			foreach (var item in items)
            {
                InventoryItem inventoryItem;

	            if (_inventoryItems.ContainsKey(item))
	            {
                    inventoryItem = _inventoryItems[item];
		            inventoryItem.transform.SetAsLastSibling();
		            inventoryItem.Count.text = item.Count.ToString();
					inventoryItems.Add(item, inventoryItem);
		            _inventoryItems.Remove(item);
				}
	            else
	            {
		            var instance = Instantiate(item.Params.Path.IsEmpty() ? ItemNoSpritesPrefab : ItemPrefab, Grid.transform);
					
                    inventoryItem = instance.GetComponent<InventoryItem>();
					inventoryItem.Item = item;
					inventoryItem.Count.text = item.Count.ToString();
					inventoryItems.Add(item, inventoryItem);
                }

                if (SelectOnRefresh) inventoryItem.Select(item == selected);
			}

			if (AddEmptyCells)
	        {
		        var columns = 0;
		        var rows = 0;

		        switch (Grid.constraint)
		        {
			        case GridLayoutGroup.Constraint.FixedColumnCount:
			        {
				        var height = Mathf.FloorToInt((ScrollRect.GetComponent<RectTransform>().rect.height + Grid.spacing.y) / (Grid.cellSize.y + Grid.spacing.y));

				        columns = Grid.constraintCount;
				        rows = Mathf.Max(height, Mathf.FloorToInt((float) items.Count / columns));

                        if (Extend) rows++;

						break;
			        }
			        case GridLayoutGroup.Constraint.FixedRowCount:
			        {
				        var width = Mathf.FloorToInt((ScrollRect.GetComponent<RectTransform>().rect.width + Grid.spacing.x) / (Grid.cellSize.x + Grid.spacing.x));

				        rows = Grid.constraintCount;
				        columns = Mathf.Max(width, Mathf.FloorToInt((float) items.Count / rows));

                        if (Extend) columns++;

						break;
			        }
		        }

		        for (var i = items.Count; i < columns * rows; i++)
		        {
			        var existing = _emptyCells.LastOrDefault();

			        if (existing != null)
			        {
				        existing.transform.SetAsLastSibling();
				        emptyCells.Add(existing);
				        _emptyCells.Remove(existing);
			        }
			        else
			        {
				        emptyCells.Add(Instantiate(CellPrefab, Grid.transform));
			        }
		        }
	        }

	        foreach (var instance in _inventoryItems.Values)
	        {
		        DestroyImmediate(instance.gameObject);
	        }

	        foreach (var instance in _emptyCells)
	        {
                DestroyImmediate(instance);
	        }

	        _inventoryItems = inventoryItems;
	        _emptyCells = emptyCells;
			_initialized = true;
		    _hash = JsonConvert.SerializeObject(items).GetHashCode();
		}
    }
}