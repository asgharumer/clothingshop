﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.HeroEditor4D.Common.CommonScripts;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Data;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Enums;
using Assets.HeroEditor4D.FantasyInventory.Scripts.Interface.Elements;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.HeroEditor4D.FantasyInventory.Scripts.Interface
{
    /// <summary>
    /// High-level inventory interface.
    /// </summary>
    public class InventoryBase : ItemWorkspace
    {
	    public ItemCollection ItemCollection;
		public Equipment Equipment;
        public ScrollInventory Bag;
        public ScrollInventory Materials;
        public Button EquipButton;
        public Button RemoveButton;
        public Button CraftButton;
        public Button UseButton;
        public AudioClip EquipSound;
        public AudioClip CraftSound;
        public AudioClip UseSound;
        public AudioSource AudioSource;
        public bool InitializeExample;

        public void Start()
        {
            if (InitializeExample)
            {
                Initialize();
                Reset();
            }
        }

        /// <summary>
        /// Initialize owned items (just for example).
        /// </summary>
        public void Initialize()
        {
            InventoryItem.OnLeftClick = SelectItem;
            InventoryItem.OnRightClick = InventoryItem.OnDoubleClick = OnDoubleClick;

            var inventory = ItemCollection.UserItems.Select(i => new Item(i.Id)).ToList(); // inventory.Clear();
			var equipped = new List<Item>();

            Bag.Initialize(ref inventory);
            Equipment.Initialize(ref equipped);
		}

        public void Initialize(ref List<Item> playerItems, ref  List<Item> equippedItems, int bagSize, Action onRefresh)
        {
            InventoryItem.OnLeftClick = SelectItem;
            InventoryItem.OnRightClick = InventoryItem.OnDoubleClick = OnDoubleClick;
            Bag.Initialize(ref playerItems);
            Equipment.Initialize(ref equippedItems);
            Equipment.SetBagSize(bagSize);
            Equipment.OnRefresh = onRefresh;
        }

        private void OnDoubleClick(Item item)
        {
            SelectItem(item);

            if (Equipment.Items.Contains(item))
            {
                Remove();
            }
            else if (CanEquip())
            {
                Equip();
            }
        }

        public void SelectItem(Item item)
        {
            SelectedItem = item;
            ItemInfo.Initialize(SelectedItem);
            Refresh();
        }

        public void Equip()
        {
            var equipped = Equipment.Items.LastOrDefault(i => i.Params.Type == SelectedItem.Params.Type);

            if (equipped != null)
            {
                AutoRemove(SelectedItem.Params.Type, Equipment.Slots.Count(i => i.Type == SelectedItem.Params.Type));
            }

            if (SelectedItem.IsTwoHanded) AutoRemove(ItemType.Shield, 1);
            if (SelectedItem.IsShield && Equipment.Items.Any(i => i.IsTwoHanded)) AutoRemove(ItemType.Weapon, 1);

            if (SelectedItem.Params.Tags.Contains(ItemTag.TwoHanded))
            {
                var shield = Equipment.Items.SingleOrDefault(i => i.Params.Type == ItemType.Shield);

                if (shield != null)
                {
                    MoveItem(shield, Equipment, Bag);
                }
            }
            else if (SelectedItem.Params.Type == ItemType.Shield)
            {
                var weapon2H = Equipment.Items.SingleOrDefault(i => i.Params.Tags.Contains(ItemTag.TwoHanded));

                if (weapon2H != null)
                {
                    MoveItem(weapon2H, Equipment, Bag);
                }
            }

            MoveItem(SelectedItem, Bag, Equipment);
            AudioSource.PlayOneShot(EquipSound);
        }

        public void Remove()
        {
            MoveItem(SelectedItem, Equipment, Bag);
            SelectItem(SelectedItem);
            AudioSource.PlayOneShot(EquipSound);
        }

        public void Craft()
        {
            var materials = MaterialList;

            if (CanCraft(materials))
            {
                materials.ForEach(i => Bag.Items.Single(j => j.Id == i.Id).Count -= i.Count);
                Bag.Items.RemoveAll(i => i.Count == 0);

                var itemId = SelectedItem.Params.FindProperty(PropertyId.Craft).Value;
                var existed = Bag.Items.SingleOrDefault(i => i.Id == itemId);

                if (existed == null)
                {
                    Bag.Items.Add(new Item(itemId));
                }
                else
                {
                    existed.Count++;
                }

                Bag.Refresh(SelectedItem);
                CraftButton.interactable = CanCraft(materials);
                AudioSource.PlayOneShot(CraftSound);

                #if TAP_HEROES

                TapHeroes.Scripts.Service.Events.Event("CraftItem", "Item", itemId);

                #endif
            }
            else
            {
                Debug.Log("No materials.");
            }
        }

        public void Use()
        {
            #if TAP_HEROES

            TapHeroes.Scripts.Data.Profile.Hero.Boosters.Clear();
            TapHeroes.Scripts.Data.Profile.Hero.Boosters.Add(new Item(SelectedItem.Id));

            #endif

            if (SelectedItem.Count == 1)
            {
                Bag.Items.Remove(SelectedItem);
                SelectedItem = Bag.Items.FirstOrDefault();

                if (SelectedItem == null)
                {
                    Bag.Refresh(null);
                    SelectedItem = Equipment.Items.FirstOrDefault();

                    if (SelectedItem != null)
                    {
                        Equipment.Refresh(SelectedItem);
                    }
                }
                else
                {
                    Bag.Refresh(SelectedItem);
                }
            }
            else
            {
                SelectedItem.Count--;
                Bag.Refresh(SelectedItem);
            }

            Equipment.OnRefresh?.Invoke();
            AudioSource.PlayOneShot(UseSound);
        }

        public override void Refresh()
        {
            if (SelectedItem == null)
            {
                ItemInfo.Reset();
                EquipButton.SetActive(false);
                RemoveButton.SetActive(false);
            }
            else
            {
                var equipped = Equipment.Items.Contains(SelectedItem);

                EquipButton.SetActive(!equipped && CanEquip());
                RemoveButton.SetActive(equipped);
                UseButton.SetActive(CanUse());
            }

            var craft = SelectedItem != null && SelectedItem.Params.Type == ItemType.Receipt;
            var materialSelected = !Bag.Items.Contains(SelectedItem) && !Equipment.Items.Contains(SelectedItem);

            CraftButton.SetActive(craft);
            Materials.SetActive(craft || materialSelected);
            Equipment.Grid.parent.SetActive(!craft && !materialSelected);

            if (craft)
            {
                var materials = MaterialList;

                Materials.Initialize(ref materials);
                CraftButton.interactable = CanCraft(materials);
            }
        }

        private List<Item> MaterialList => SelectedItem.Params.FindProperty(PropertyId.Materials).Value.Split(',').Select(i => i.Split(':')).Select(i => new Item(i[0], int.Parse(i[1]))).ToList();

        private bool CanEquip()
        {
            return Bag.Items.Contains(SelectedItem) && Equipment.Slots.Any(i => i.Type == SelectedItem.Params.Type && (i.Class == ItemClass.Unknown || i.Class == SelectedItem.Params.Class)) && SelectedItem.Params.Class != ItemClass.Booster;
        }

        private bool CanUse()
        {
            return SelectedItem.Params.Class == ItemClass.Booster;
        }

        private bool CanCraft(List<Item> materials)
        {
            return materials.All(i => Bag.Items.Any(j => j.Id == i.Id && j.Count >= i.Count));
        }

        /// <summary>
        /// Automatically removes items if target slot is busy.
        /// </summary>
        private void AutoRemove(ItemType itemType, int max)
        {
            var items = Equipment.Items.Where(i => i.Params.Type == itemType).ToList();
            long sum = 0;

            foreach (var p in items)
            {
                sum += p.Count;
            }

            if (sum == max)
            {
                MoveItem(items.LastOrDefault(i => i != SelectedItem) ?? items.Last(), Equipment, Bag, silent: true);
            }
        }
    }
}