﻿using UnityEngine;

namespace Assets.HeroEditor4D.FantasyInventory.Scripts
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        public void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}